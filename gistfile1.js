exports.convertXLSXToJSON2 = function(req, res) {
    var path = fileServerPath + req.body.name;
    console.log("starting..." + path);
    var exists = fs.existsSync(path);
    console.log("path exists? " + exists);
    console.log("in xlsx to json 2 method...starting conversion");
    try {
        var obj = asyncXLSX.parse(path);
        console.log("obj: " + obj);
        res.send(obj);
        console.log("end conversion")
    }
    catch(ex) {
        //console.log("throwing error");
        console.log("throwing error: " + ex.message);
        res.send(ex.message);
    }
};